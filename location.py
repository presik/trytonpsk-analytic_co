from trytond.pool import PoolMeta, Pool
from trytond.model import fields
from trytond.pyson import Eval
from trytond.transaction import Transaction

class Location(metaclass=PoolMeta):
    "Stock Location"
    __name__ = 'stock.location'

    analytic_account = fields.Many2One('analytic_account.account',
        'Analytic Account', domain=[
            ('type', 'in', ['normal', 'distribution']),
            ('company', '=', Eval('context', {}).get('company', -1))
        ], states={'invisible': Eval('type') == 'storage_location'})