# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond.model import ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.wizard import Button, StateReport, StateView, Wizard


class Account(metaclass=PoolMeta):
    "Analytic Account"
    __name__ = 'analytic_account.account'

    def get_rec_name(self, name):
        rec_name = super(Account, self).get_rec_name(name)
        if self.parent:
            return self.parent.get_rec_name(name) + ' / ' + rec_name
        else:
            return rec_name

    @classmethod
    def search_rec_name(cls, name, clause):
        domain = super(Account, cls).search_rec_name(name, clause)
        domain.extend([
            ('parent.name',) + tuple(clause[1:]),
            ('parent.parent.name',) + tuple(clause[1:]),
            ('parent.parent.parent.name',) + tuple(clause[1:]),
        ])
        return domain


class AnalyticReportStart(ModelView):
    "Analytic Report Start"
    __name__ = 'analytic_co.reporting.start'
    detailed = fields.Boolean('Analytic Account Detailed')


class AnalyticAccountWizard(Wizard):
    "Analytic Account Wizard"
    __name__ = 'analytic_co.reporting_wizard'
    start = StateView('analytic_co.reporting.start',
        'analytic_co.analytic_co_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok'),
            ])
    print_ = StateReport('analytic_account.account_report')

    def do_print_(self, action):
        id_ = Transaction().context['active_id']
        try:
            start_date = Transaction().context['start_date']
            end_date = Transaction().context['end_date']
        except:
            raise UserError(
                    gettext("""Error: debe imprimir el reporte desde
                    Contabilidad/Planes Contables/Abrir plan analitico"""))
        detailed = self.start.detailed
        data = {
            'start_date': start_date,
            'end_date': end_date,
            'active_id': id_,
            'detailed': detailed,
        }

        return action, data

    def transition_print_(self):
        return 'end'


class AnalyticAccountReport(Report):
    __metaclass__ = PoolMeta
    __name__ = "analytic_account.account_report"

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        AnalyticAccount = Pool().get('analytic_account.account')
        Company = Pool().get('company.company')
        Line = Pool().get('analytic_account.line')
        with Transaction().set_context(data):
            records = [AnalyticAccount(data['active_id'])]
        targets = []
        while records:
            accounts = records.pop()
            if not isinstance(accounts, tuple):
                accounts = [accounts]
            for account in accounts:

                lines_ = []

                if account.childs:
                    records.append(account.childs)
                else:
                    dom = [('account', '=', account.id)]
                    if data['start_date']:
                        dom.append(('date', '>=', data['start_date']))
                    if data['end_date']:
                        dom.append(('date', '<=', data['end_date']))
                    lines = Line.search_read(dom, fields_names=[
                        'move_line.account',
                        'move_line.account.code',
                        'move_line.account.name',
                        'move_line.debit',
                        'move_line.credit',
                    ])
                    if lines:
                        grouped = {}
                        for line in lines:
                            if line['move_line.']['account.']['code'] not in grouped:
                                grouped[line['move_line.']['account.']['code']] = {
                                    'code': line['move_line.']['account.']['code'],
                                    'account': line['move_line.']['account.']['name'],
                                    'debit': [],
                                    'credit': [],
                                    'balance': [],
                                }
                            debit = line['move_line.']['debit']
                            credit = line['move_line.']['credit']
                            balance = debit - credit
                            grouped[line['move_line.']['account.']['code']]['balance'].append(
                                balance,
                            )
                            grouped[line['move_line.']['account.']['code']]['credit'].append(
                                credit,
                            )
                            grouped[line['move_line.']['account.']['code']]['debit'].append(
                                debit,
                            )
                        lines_ = grouped.values()
                value = {
                    'account': account,
                    'lines': lines_,
                    }
                targets.append(value)
        report_context['records'] = targets
        report_context['company'] = Company(Transaction().context.get('company'))
        report_context['data'] = data
        return report_context


class LineDetailedReport(Report):
    "Line Detailed Report "
    __name__ = "analytic_account.line.detailed_report"

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        Line = Pool().get('analytic_account.line')
        AnalyticAccount = Pool().get('analytic_account.account')
        MoveLine = Pool().get('account.move.line')
        Company = Pool().get('company.company')
        context = Transaction().context
        company = Company(context['company'])
        dom = []
        try:
            start_date = context['start_date']
            end_date = context['end_date']
        except Exception:
            raise UserError(
                    gettext("""Error: debe imprimir el reporte desde
                    Contabilidad/Planes Contables/Abrir plan analitico"""))

        if start_date:
            dom.append(('move_line.move.date', '>=', context['start_date']))
        if end_date:
            dom.append(('move_line.move.date', '<=', context['end_date']))

        fields_names = [
            'move_line.account',
            'move_line.account.code',
            'move_line.account.name',
            'debit',
            'credit',
            'move_line.move_origin.rec_name',
            'move_line.description',
            'move_line.reference',
            'move_line.party.name',
            'move_line.party.id_number',
            'move_line.move.number',
            'move_line.move.description',
            'move_line.move.date',
        ]

        fields = MoveLine.fields_get(fields_names=['operation_center'])
        if 'operation_center' in fields.keys():
            fields_names.append('move_line.operation_center.rec_name')

        accounts = AnalyticAccount.search_read([
            ('id', 'in', data['ids']),
        ], fields_names=['name', 'code', 'debit', 'credit', 'balance'])

        records = {p['id']: p for p in accounts}
        for d in data['ids']:
            domain = dom.copy()
            domain.append(('account', '=', d))
            lines = Line.search_read(domain, fields_names=fields_names)
            records[d]['lines'] = lines

        report_context['records'] = records.values()
        report_context['company'] = company
        report_context['context'] = context
        return report_context
