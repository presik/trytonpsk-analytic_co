import polars as pl
from trytond.transaction import Transaction

schema_cash_flow = {
    'parent_name': pl.String,
    'name': pl.String,
    'account_code': pl.String,
    'account_name': pl.String,
    'date': pl.Date,
    'total': pl.Float32,
}


def polar_query(query, args, schema=None):
    cursor = Transaction().connection.cursor()
    _query = query.format(**args)
    return pl.read_database(_query, cursor, infer_schema_length=None, schema_overrides=schema)
