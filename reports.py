from trytond.model import ModelView, fields
from trytond.wizard import Button, StateReport, StateView, Wizard
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.pyson import Eval
from trytond.report import Report

from .helper import polar_query
from .queries import income_analytic


class IncomeStatementByAnalyticStart(ModelView):
    "Income Statement By Analytic Start"
    __name__ = 'analytic_co.income_statement_by_analytic.start'
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
            required=True)
    start_period = fields.Many2One('account.period', 'Start Period',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
            ('type', '=', 'standard'),
        ], depends=['fiscalyear'], required=True)
    end_period = fields.Many2One('account.period', 'End Period',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
            ('type', '=', 'standard'),
        ], depends=['fiscalyear'], required=True)
    company = fields.Many2One('company.company', 'Company', required=True)
    analytic = fields.Many2One('analytic_account.account', 'Analytic', required=True)
    detailed = fields.Boolean('Detailed')

    @staticmethod
    def default_posted():
        return False

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @fields.depends('fiscalyear')
    def on_change_fiscalyear(self):
        self.start_period = None
        self.end_period = None

    @fields.depends('start_period')
    def on_change_start_period(self):
        self.end_period = self.start_period.id


class IncomeStatementByAnalytic(Wizard):
    "Income Statement By Analytic"
    __name__ = 'analytic_co.income_statement_by_analytic'
    start = StateView(
        'analytic_co.income_statement_by_analytic.start',
        'analytic_co.income_statement_by_analytic_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-print', default=True),
        ])
    print_ = StateReport('analytic_co.income_statement_by_analytic.report')

    def do_print_(self, action):
        Period = Pool().get('account.period')
        periods = Period.search([
            ('start_date', '>=', self.start.start_period.start_date),
            ('end_date', '<=', self.start.end_period.end_date),
            ('type', '=', 'standard'),
        ])

        periods_ids = [p.id for p in periods]
        data = {
            'fiscalyear': self.start.fiscalyear.id,
            'periods': periods_ids,
            'start_period': self.start.start_period.id,
            'end_period': self.start.end_period.id,
            'company': self.start.company.id,
            'analytic': self.start.analytic.id,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class IncomeStatementByAnalyticReport(Report):
    __name__ = 'analytic_co.income_statement_by_analytic.report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        Period = pool.get('account.period')
        Account = pool.get('analytic_account.account')
        company = Company(data['company'])
        # analytic = [Account(data['analytic'])]
        analytic = Account(data['analytic'])
        periods_ids = data['periods']
        if len(periods_ids) > 1:
            periods = str(tuple(periods_ids))
        else:
            periods = f"({periods_ids[0]})"

        analytics = f"({analytic.id})"
        # if len(analytic) > 1:
        #     analytics = str(tuple(periods_ids))
        # else:

        args = {
            # 'start_date': start_date,
            # 'end_date': end_date,
            'periods': periods,
            'analytics': analytics,
        }
        df_ana = polar_query(income_analytic, args)
        _records = df_ana.rows_by_key(key='id', include_key=True, named=True)
        records = {}

        def _set_recursive_balance(parents):
            up_parents = {}
            for parent_key, values in parents.items():
                if not records.get(parent_key):
                   continue
                balance = sum(values)
                records[parent_key]['balance'] = balance
                parent = records[parent_key]['parent']
                try:
                    up_parents[parent].append(balance)
                except:
                    up_parents[parent] = [balance]
            if up_parents:
                _set_recursive_balance(up_parents)

        parents = {}
        for acc_id, (rec, ) in _records.items():
            if rec['balance']:
                try:
                    parents[rec['parent']].append(rec['balance'])
                except:
                    parents[rec['parent']] = [rec['balance']]
            rec['name'] = (len(rec['code']) - 1) * '-' + rec['name']
            records[acc_id] = rec

        _set_recursive_balance(parents)
        report_context['records'] = records.values()
        report_context['start_period'] = Period(data['start_period'])
        report_context['end_period'] = Period(data['end_period'])
        report_context['result'] = 0
        report_context['company'] = company
        report_context['analytic'] = analytic.name
        return report_context
