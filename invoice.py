# This file is part of Tryton. The COPYRIGHT file at the top level
# of this repository contains the full copyright notices and license terms.
import contextlib
from decimal import Decimal

from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond.model import ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import And, Eval
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.wizard import Button, StateReport, StateTransition, StateView, Wizard

_ZERO = Decimal('0.0')


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'

    @classmethod
    def __setup__(cls):
        super(Invoice, cls).__setup__()
        cls._buttons.update({
            'add_analytic': {
                'invisible': Eval('state') != 'draft',
            },
        })

    @classmethod
    @ModelView.button_action('analytic_co.wizard_add_analytic')
    def add_analytic(cls, records):
        pass

    @classmethod
    def validate_invoice(cls, invoices):
        cls.validate_require_analytic([i for i in invoices if i.type == 'in'])
        super(Invoice, cls).validate_invoice(invoices)

    @classmethod
    def validate_require_analytic(cls, invoices):
        def is_required(line):
            return line.account and line.account.type.statement != 'balance' and not line.analytic_account
        if any(is_required(ln) for invoice in invoices for ln in invoice.lines):
            raise UserError(gettext('analytic_co.msg_analytic_error_invoice'))


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    analytic_account = fields.Many2One('analytic_account.account',
        'Analytic Account', domain=[
            ('type', 'in', ['normal', 'distribution']),
            ('company', '=', Eval('context', {}).get('company', -1)),
        ], states={
            'readonly': Eval('invoice_state').in_(['posted', 'paid']),
            'required': And(Eval('invoice_type') == 'in', Eval('require_analytic', False)),
        },
        depends=['account', 'require_analytic', 'invoice_state'])
    require_analytic = fields.Function(fields.Boolean(
            'Require Analytic'),
        'on_change_with_require_analytic')

    def _credit(self):
        line = super(InvoiceLine, self)._credit()
        line.analytic_account = self.analytic_account
        return line

    @fields.depends('analytic_accounts', 'analytic_account')
    def on_change_analytic_account(self):
        if self.analytic_account:
            for ac in self.analytic_accounts:
                ac.account = self.analytic_account.id
        else:
            for ac in self.analytic_accounts:
                ac.account = None

    @fields.depends('analytic_account', 'account', 'invoice_state')
    def on_change_with_require_analytic(self, name=None):
        required = False
        if self.account and self.account.type.statement != 'balance' and self.invoice_state != 'draft':
            required = True
        return required

    def get_move_lines(self):
        lines = super(InvoiceLine, self).get_move_lines()
        analytic_id = None
        with contextlib.suppress(Exception):
            if hasattr(self.invoice, 'shop') and self.invoice.shop and hasattr(self.product, 'get_analytic'):
                analytic_id = self.product.get_analytic(self.invoice.shop, 'expenses')
        for line in lines:
            if line.account.code.startswith('6') and analytic_id:
                date = self.invoice.accounting_date or self.invoice.invoice_date
                analytic_lines = []
                for entry in self.analytic_accounts:
                    analytic_lines.extend(
                        entry.get_analytic_lines(line, date))
                for an in analytic_lines:
                    an.account = analytic_id
                line.analytic_lines = analytic_lines
            elif line.account.code[0] in ('1', '2', '3'):
                line.analytic_lines = None

        return lines


class InvoiceCharge(metaclass=PoolMeta):
    "Invoice Charge"
    __name__ = "invoice.charge"
    analytic_account = fields.Many2One('analytic_account.account',
        'Analytic Account', domain=[
            ('type', 'in', ['normal', 'distribution']),
            ('company', '=', Eval('context', {}).get('company', -1)),
        ])


class AnalyticInvoiceStart(ModelView):
    "Analytic Invoice Report Start"
    __name__ = 'analytic_co.invoice.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    type_inv = fields.Selection([
            ('out', 'Invoice'),
            ('out_credit_note', 'Out Credit Note'),
            ('in', 'Supplier Invoice'),
            ('in_credit_note', 'In Credit Note'),
        ], 'Type', required=True)
    start_date = fields.Date("Start Date", required=True)
    end_date = fields.Date("End Date", required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_end_date():
        Date = Pool().get('ir.date')
        return Date.today()


class AnalyticInvoice(Wizard):
    "Analytic Invoice Report"
    __name__ = 'analytic_co.invoice'
    start = StateView(
        'analytic_co.invoice.start',
        'analytic_co.analytic_invoice_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateReport('analytic_co.invoice.report')

    def do_print_(self, action):
        data = {
            'company': self.start.company.id,
            'type_invoice': self.start.type_inv,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            }
        return action, data

    def transition_print_(self):
        return 'end'


class AnalyticInvoiceReport(Report):
    __name__ = 'analytic_co.invoice.report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        Lines = pool.get('account.invoice.line')
        LineTax = pool.get('account.invoice.line-account.tax')
        if data['type_invoice'] == "out_credit_note":
            type_invoice = 'out'
        elif data['type_invoice'] == "in_credit_note":
            type_invoice = 'in'
        else:
            type_invoice = data['type_invoice']

        lines = Lines.search([
            ('invoice.company', '=', data['company']),
            ('invoice.type', '=', type_invoice),
            ('invoice.invoice_date', '>=', data['start_date']),
            ('invoice.invoice_date', '<=', data['end_date']),
            ('invoice.state', 'in', ['validated', 'posted', 'paid']),
        ])

        invoice_lines_filtered = []

        def get_parent_names(account, list_names, names=''):
            if account and account.parent:
                list_names.append(account.parent.name)
                get_parent_names(account.parent, list_names)
            i = len(list_names) - 1
            for l in list_names:
                names += list_names[i] + ' - '
                i -= 1
            return names

        for line in lines:
            lines_taxes = LineTax.search([
                ('line', '=', line.id),
            ])
            _line = {'line': line}
            iva_amount = 0
            renta_amount = 0
            other_amount = 0
            for line_tax in lines_taxes:
                if line_tax.tax.classification:
                    if line_tax.tax.classification == 'iva':
                        iva_amount = line.unit_price * line_tax.tax.rate
                    elif line_tax.tax.classification == 'renta':
                        renta_amount = line.unit_price * line_tax.tax.rate
                    elif line_tax.tax.type == 'percentage' and line_tax.tax.rate:
                        other_amount += line.unit_price * line_tax.tax.rate
                    elif line_tax.tax.rate:
                        other_amount += line_tax.tax.rate
                    else:
                        other_amount += 0
                else:
                    if line_tax.tax.type == 'fixed' and line_tax.tax.rate:
                        other_amount += line_tax.tax.rate
                    if line_tax.tax.rate and line_tax.tax.type == 'percentage':
                        other_amount += line.unit_price * line_tax.tax.rate
                    else:
                        other_amount += 0
            _line['iva'] = iva_amount
            _line['renta'] = renta_amount
            _line['other_tax'] = other_amount

            name_analytic = ''
            code = ''
            if line.analytic_accounts:
                for a in line.analytic_accounts:
                    code_name = ''
                    if a.account:
                        code = a.account.code
                        code_name = get_parent_names(a.account, [a.account.name])
                    name_analytic = name_analytic + code_name
            _line['analytic'] = name_analytic
            _line['analytic_code'] = code

            if line.invoice.untaxed_amount < _ZERO:
                if "credit" in data['type_invoice']:
                    invoice_lines_filtered.append(_line)
                else:
                    continue
            else:
                if "credit" not in data['type_invoice']:
                    invoice_lines_filtered.append(_line)
                else:
                    continue

        report_context['records'] = invoice_lines_filtered
        report_context['company'] = Company(data['company'])
        report_context['start'] = data['start_date']
        report_context['end'] = data['end_date']

        return report_context


class AddAnalyticAccountStart(ModelView):
    "Add Analytic Account Start"
    __name__ = 'account_invoice.add_analytic.start'
    analytic_account = fields.Many2One('analytic_account.account',
        'Analytic Account', required=True, domain=[
            ('type', '=', 'normal'),
        ])


class AddAnalyticAccount(Wizard):
    "Add Analytic Account"
    __name__ = 'account_invoice.add_analytic'
    start = StateView(
        'account_invoice.add_analytic.start',
        'analytic_co.view_wizard_add_analytic_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    def transition_accept(self):
        id_ = Transaction().context['active_id']
        Invoice = Pool().get('account.invoice')
        invoice = Invoice(id_)

        account_id = self.start.analytic_account.id

        for line in invoice.lines:
            if line.type != 'line':
                continue
            line.analytic_account = account_id
            line.save()
            for la in line.analytic_accounts:
                la.account = account_id
                la.save()
        return 'end'
