# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool

from . import analytic
from . import invoice
from . import location
from . import rule
from . import stock
from . import voucher
from . import reports


def register():
    Pool.register(
        invoice.InvoiceLine,
        invoice.InvoiceCharge,
        rule.Rule,
        analytic.AnalyticAccountReport,
        analytic.Account,
        invoice.Invoice,
        invoice.AnalyticInvoiceStart,
        invoice.AddAnalyticAccountStart,
        analytic.AnalyticReportStart,
        voucher.VoucherLine,
        voucher.Note,
        voucher.NoteLine,
        voucher.BankConceptStatement,
        voucher.AnalyticAccountEntry,
        voucher.AnalyticVoucherStart,
        stock.ShipmentIn,
        stock.ShipmentInternal,
        stock.ShipmentOut,
        stock.Invetory,
        location.Location,
        reports.IncomeStatementByAnalyticStart,
        module='analytic_co', type_='model')
    Pool.register(
        analytic.AnalyticAccountReport,
        invoice.AnalyticInvoiceReport,
        analytic.LineDetailedReport,
        voucher.AnalyticVoucherReport,
        reports.IncomeStatementByAnalyticReport,
        module='analytic_co', type_='report')
    Pool.register(
        analytic.AnalyticAccountWizard,
        invoice.AnalyticInvoice,
        voucher.AnalyticVoucher,
        invoice.AddAnalyticAccount,
        reports.IncomeStatementByAnalytic,
        module='analytic_co', type_='wizard')
