SELECT
    id,
    code,
    name,
    parent
FROM
    account_account
WHERE
    code SIMILAR TO '4%|5%|6%|7%'
ORDER BY
    code;

SELECT
    acc.id,
    acc.code,
    acc.name,
    acc.parent,
    mli.debit,
    mli.credit,
    mli.credit - mli.debit AS balance
FROM
    account_account AS acc
    LEFT JOIN (
        SELECT
            ml.account,
            SUM(ml.debit) as debit,
            SUM(ml.credit) AS credit
        FROM
            account_move_line AS ml
            JOIN account_move AS mv ON ml.move = mv.id
            LEFT JOIN analytic_account_line AS aa ON aa.move_line = ml.id
        WHERE
            ml.account IN (
                SELECT
                    id
                FROM
                    account_account
                WHERE
                    code SIMILAR TO '4%|5%|6%|7%'
            )
            AND mv.period >= 37
            AND aa.account IN (74, 57, 71, 72)
        GROUP BY
            ml.account
    ) AS mli ON mli.account = acc.id
WHERE
    acc.code SIMILAR TO '4%|5%|6%|7%'
ORDER BY
    acc.code;
