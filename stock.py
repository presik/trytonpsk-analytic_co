from trytond.model import fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval
from trytond.transaction import Transaction


class ShipmentIn(metaclass=PoolMeta):
    "Shipment In"
    __name__ = 'stock.shipment.in'
    analytic_account = fields.Many2One('analytic_account.account',
        'Analytic Account', domain=[
            ('type', 'in', ['normal', 'distribution']),
            ('company', '=', Eval('context', {}).get('company', -1)),
        ])

    @classmethod
    def receive(cls, shipments):
        for shipment in shipments:
            # context = Transaction().context
            ctx = {
                'analytic_account': shipment.analytic_account.id if shipment.analytic_account else None,
                }
            with Transaction().set_context(ctx):
                super(ShipmentIn, cls).receive([shipment])


class ShipmentInternal(metaclass=PoolMeta):
    "Shipment Internal"
    __name__ = 'stock.shipment.internal'
    analytic_account = fields.Many2One('analytic_account.account',
        'Analytic Account', domain=[
            ('type', 'in', ['normal', 'distribution']),
            ('company', '=', Eval('context', {}).get('company', -1)),
        ], states={
            'required': True,
        })

    @fields.depends('to_location', 'analytic_account')
    def on_change_to_location(self, name=None):
        if self.to_location and self.to_location.analytic_account and not self.analytic_account:
            self.analytic_account = self.to_location.analytic_account.id
        else:
            self.analytic_account = None

    @classmethod
    def create(cls, vlist):
        Location = Pool().get('stock.location')
        vlist = [v.copy() for v in vlist]
        for values in vlist:
            if values.get('to_location') and not values.get('analytic_account'):
                values['analytic_account'] = Location(values['to_location']).analytic_account
        return super().create(vlist)


class ShipmentOut(metaclass=PoolMeta):
    "Shipment Out"
    __name__ = 'stock.shipment.out'

    analytic_account = fields.Many2One('analytic_account.account',
        'Analytic Account',
        domain=[
            ('type', 'in', ['normal', 'distribution']),
            ('company', '=', Eval('context', {}).get('company', -1)),
        ])


class Invetory(metaclass=PoolMeta):
    "Stock  Inventory"
    __name__ = 'stock.inventory'

    analytic_account = fields.Many2One('analytic_account.account',
        'Analytic Account',
        domain=[
            ('type', 'in', ['normal', 'distribution']),
            ('company', '=', Eval('context', {}).get('company', -1)),
        ],
        states={
            'required': Eval('state') == 'draft',
            'readonly': Eval('state') != 'draft',
        },
        )
